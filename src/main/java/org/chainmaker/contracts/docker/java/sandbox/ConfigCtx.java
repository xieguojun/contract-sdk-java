package org.chainmaker.contracts.docker.java.sandbox;

import java.util.Objects;

public class ConfigCtx {
    public static String processName;
    public static String contractName;
    public static String contractFilePath;
    public static String logLevel;
    public static String runtimeServiceHost;
    public static int runtimeServicePort;

    public static int engineServicePort;

    public static boolean disableSlowLog = false;


    public static void loadConfig(String args[]){
        processName = args[0];
        contractName = args[1];
        logLevel = args[2];
        runtimeServiceHost = args[3];
        runtimeServicePort = Integer.parseInt(args[4]);
        engineServicePort = Integer.parseInt(args[5]);
        disableSlowLog = Boolean.parseBoolean(args[6]);
    };
}
