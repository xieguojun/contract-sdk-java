package org.chainmaker.contracts.docker.java.sandbox.kviterator;

public class KeyModification {
    private String Key;
    private String Field;
    private byte[] Value;
    private String TxId;
    private int BlockHeight;
    private boolean IsDelete;
    private String Timestamp;

    public KeyModification(String key, String field, byte[] value, String txId, int blockHeight, boolean isDelete, String timestamp) {
        Key = key;
        Field = field;
        Value = value;
        TxId = txId;
        BlockHeight = blockHeight;
        IsDelete = isDelete;
        Timestamp = timestamp;
    }

    public String getKey() {
        return Key;
    }

    public String getField() {
        return Field;
    }

    public byte[] getValue() {
        return Value;
    }

    public String getTxId() {
        return TxId;
    }

    public int getBlockHeight() {
        return BlockHeight;
    }

    public boolean isDelete() {
        return IsDelete;
    }

    public String getTimestamp() {
        return Timestamp;
    }
}
