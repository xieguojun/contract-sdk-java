package org.chainmaker.contracts.docker.java.sandbox;

import org.chainmaker.contracts.docker.java.pb.proto.Response;

public interface IContract {

    default Response initContract(){ return SDK.success("init success".getBytes()); }


    default Response upgradeContract(){ return SDK.success("upgrade success".getBytes()); }

}
