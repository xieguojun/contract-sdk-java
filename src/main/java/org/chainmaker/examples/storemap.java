package org.chainmaker.examples;

import com.alibaba.fastjson.JSON;
import org.chainmaker.contracts.docker.java.pb.proto.Response;
import org.chainmaker.contracts.docker.java.sandbox.*;
import org.chainmaker.contracts.docker.java.sandbox.annotaion.ContractMethod;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.KVResult;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.ResultSetKV;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;


public class storemap implements IContract {

    @ContractMethod
    public Response set() throws IOException, ContractException, NoSuchAlgorithmException, InterruptedException {
        long deep = 3;
        String m = "m1";

        StoreMap storeMap = StoreMap.NewStoreMap(m, deep);

        String[] keys = {"key1", "key2", "key3"};
        byte[] value = "123value".getBytes();

        storeMap.set(keys, value);

        return SDK.success("success");
    }

    @ContractMethod
    public Response get() throws IOException, InterruptedException, ContractException, NoSuchAlgorithmException {
        long deep = 3;
        String m = "m1";

        StoreMap storeMap = StoreMap.NewStoreMap(m, deep);

        String[] keys = {"key1", "key2", "key3"};
        byte[] value = storeMap.get(keys);

        return SDK.success(value);
    }

    @ContractMethod
    public Response del() throws IOException, ContractException, NoSuchAlgorithmException, InterruptedException {

        long deep = 3;
        String m = "m1";

        StoreMap storeMap = StoreMap.NewStoreMap(m, deep);

        String[] keys = {"key1", "key2", "key3"};

        storeMap.del(keys);

        return SDK.success("delete success");
    }


    @ContractMethod
    public Response exist() throws IOException, ContractException, NoSuchAlgorithmException, InterruptedException {

        long deep = 3;
        String m = "m1";

        StoreMap storeMap = StoreMap.NewStoreMap(m, deep);

        String[] keys = {"key1", "key2", "key3"};

        boolean value = storeMap.exist(keys);

        return SDK.success(String.valueOf(value));
    }


    @ContractMethod
    public Response setKeys() throws IOException, ContractException, NoSuchAlgorithmException, InterruptedException {
        long deep = 3;
        String m = "m1";

        StoreMap storeMap = StoreMap.NewStoreMap(m, deep);

        String keysStr = SDK.getParam("keys");
        String[] keys = keysStr.split(",", (int)deep);
        byte[] value = SDK.getParamBytes("value");

        storeMap.set(keys, value);

        return SDK.success("success");
    }


    @ContractMethod
    public Response iter() throws IOException, ContractException, NoSuchAlgorithmException, InterruptedException {

        long deep = 3;
        String m = "m1";

        StoreMap storeMap = StoreMap.NewStoreMap(m, deep);

        String[] keys = {"key1", "key2"};

        ResultSetKV iter = storeMap.newStoreMapIteratorPrefixWithKey(keys);

        while(iter.hasNext()){
            KVResult result = iter.next();
            SDK.logI(JSON.toJSONString(result));
        }
        iter.close();

        return SDK.success("iter success");
    }
    public static void main(String[] args) {
        Sandbox.serve(args, new storemap());
    }

}
